//---------- Hero --> Mage,Paladin --> theMage, thePaladin-----------------\\
//Building our initial Hero Class
function Hero(name, hp) {
    this.name = name;
    this.hp = hp;
}
//Buildling our Mage Class -- which will inherit from its prototype (Hero constructor)
function Mage(name, hp, spell) {
    Hero.call(this, name, hp); //CALL on this constructor function to inherit properties

    this.spell = spell; //might need to change this to weapon as well; or spell = healing spell which is also method
};
//Buildling our Paladin Class 
function Paladin(name, hp, weapon) {
    Hero.call(this, name, hp);

    this.weapon = weapon;
};

//Giving our Mage class a few methods...
Mage.prototype.attack1 = function() {
    console.log('You have been struck by a sword!  -20HP')
};
Mage.prototype.attack2 = function() {
    console.log('You have been struck by a shield!  -10HP')
};
Mage.prototype.heal = function() {
    console.log('You have cast a healing spell.  +10HP')
};
//Giving our Paladin class a few methods...
Paladin.prototype.attack1 = function() {
    console.log('You have been hit by a fireball!  -20HP')
};
Paladin.prototype.attack2 = function() {
    console.log('You have been hit by a lightning bolt!  -10HP')
};
Paladin.prototype.heal = function() {
    console.log('You have applied a bandage.  +10HP')
};
//Setting up prototype Inheritance.  Linking up the two prototypes (Mage and Hero prototypes)
Mage.prototype = Object.create(Hero.prototype);
Mage.prototype.constructor = Mage;

Paladin.prototype = Object.create(Hero.prototype);
Paladin.prototype.constructor = Paladin;

//Mage --> theMage        Paladin --> thePaladin
const theMage = new Mage('Arnie the Mage', 50, 'Healing'); //(name, hp --> Hero constructor; spell -->NEWMage)
const thePaladin = new Paladin('Rynard the Paladin', 50, 'Sword');

//**----------------------------------------------------------------------------**/
const methods = ['attack1', 'attack2', 'heal'] //click-event will target one of these methods
const output = document.getElementById('output') //where we want to output our battle -->also could have used document.getElementById('#battle-output')[0]
const mage = document.getElementById('mage')
const paladin = document.getElementById('paladin')
const mageHP = document.getElementById('mage-hp')
const paladinHP = document.getElementById('paladin-hp')
let player = 1;

function randomNum() {
    return Math.floor(Math.random() * 3)
}

function startCombat(event) { //event = click;  event.target.id = element(image).id
    console.log(`It is player ${player}'s turn.`)
    if (player === 1 && event.target.id === 'mage') {
        let action = methods[randomNum()];
        //console.log(action)
        if (action === 'attack1') {
            output.innerHTML = `${theMage.name} has been struck by a sword!  -20HP`;
            theMage.hp -= 20;
            mageHP.innerHTML = theMage.hp;
        }
        if (action === 'attack2') {
            output.innerHTML = `${theMage.name} has been struck by a shield!  -10HP`;
            theMage.hp -= 10;
            mageHP.innerHTML = theMage.hp;
        }
        if (action === 'heal') {
            output.innerHTML = `${theMage.name} has cast a healing spell.  +10HP`;
            theMage.hp += 10;
            mageHP.innerHTML = theMage.hp;
        }

        checkWin();
        player += 1; //Should switch player to player 2

    }

    if (player === 2 && event.target.id === 'paladin') {
        let action = methods[randomNum()];
        //console.log(action)
        if (action === 'attack1') {
            output.innerHTML = `${thePaladin.name} has been hit by a fireball!  -20HP`;
            thePaladin.hp -= 20;
            paladinHP.innerHTML = thePaladin.hp;
        }
        if (action === 'attack2') {
            output.innerHTML = `${thePaladin.name} has been hit by a lightning bolt!  -10HP`;
            thePaladin.hp -= 10;
            paladinHP.innerHTML = thePaladin.hp;
        }
        if (action === 'heal') {
            output.innerHTML = `${thePaladin.name} has applied a bandage.  +10HP`;
            thePaladin.hp += 10;
            paladinHP.innerHTML = thePaladin.hp;
        }
        checkWin();
        player -= 1;
    } else {
        alert('It is not your turn.  Please click the other combatant.')
    }
}

//Now we add our 'click event' and pass in the startCombat function as the callback

mage.addEventListener('click', startCombat) //callback function is set to startCombat (*NOTE -- didn't have to include parameter)
paladin.addEventListener('click', startCombat)

// function oldCheckWin() {
//     if (thePaladin.hp <= 0 || theMage.hp <= 0) {
//         alert('You have been slain!');
//         thePaladin.hp = 50;
//         theMage.hp = 50;
//         mageHP.innerHTML = theMage.hp;
//         paladinHP.innerHTML = thePaladin.hp;
//         output.innerHTML = 'Next Round.   FIGHT!'
//     }
// }

let paladinWins = 0;
let mageWins = 0;

function checkWin() {
    if (thePaladin.hp <= 0) {
        alert('You have been slain!');
        mageWins += 1;
        console.log(`${theMage.name} has won ${mageWins} game(s).`)
        thePaladin.hp = 50;
        theMage.hp = 50;
        mageHP.innerHTML = theMage.hp;
        paladinHP.innerHTML = thePaladin.hp;
        output.innerHTML = 'Next Round.   FIGHT!'

    }
    if (theMage.hp <= 0) {
        alert('You have been slain!');
        paladinWins += 1;
        console.log(`${thePaladin.name} has won ${paladinWins} game(s).`)
        thePaladin.hp = 50;
        theMage.hp = 50;
        mageHP.innerHTML = theMage.hp;
        paladinHP.innerHTML = thePaladin.hp;
        output.innerHTML = 'Next Round.   FIGHT!'
    }
}

//Could include additional 'scoreboard' to keep track of each win
//would include this to the left & right of the HP-count scoreboard
//Also could include names of the characters above their images.  
//Or could allow the player to select which character he would prefer to be, by creating a prompt that the User will select either 1 or 2
//1 being the Mage, 2 being the Paladin